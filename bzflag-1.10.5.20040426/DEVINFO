Information for the BZFlag Developer
====================================

BZFlag source code lives in the following directories:

game independent, platform independent libraries
------------------------------------------------
common		- general utility
geometry	- geometric mathematics
obstacle	- collision detection
scene		- scene graph
ogl		- wrapper classes for OpenGL

game independent, platform dependent libraries
----------------------------------------------
net		- networking
platform	- general platform dependant code

applications
------------
bzflag		-- the BZFlag client
bzfls		-- the BZFlag meta-server (i.e. the list of servers server)
bzfs		-- the BZFlag server
bzfrelay	-- the BZFlag relay (for relaying to servers)
bzadmin		-- the BZFlag admin tool and chat client

Note that only a few directories contain code directly related to the game
BZFlag.  In particular: bzflag, bzfls, bzadmin, and bzfs. (And obstacle, but
that will hopefully be made more general some day.) The rest of the code could
potentially be used in a different game. If your code is BZFlag specific then
it should go into one of the above directories (game if used by more than one
app, otherwise into the appropriate app directory).  If not then put it in one
of the other directories or add a new directory.


Coding conventions
==================

If you plan on contributing any source code to BZFlag, we would like you to
follow these conventions.  Contributions that don't conform are likely to be
rejected until they do.

Code Organization
-----------------
Follow the above organization when introducing new files.  Any code that would
potentially be useful in another game goes outside of the app directories and
outside the game directory.  Platform dependent code normally goes into
platform, net.

Header files that are private to a library go into that library's directory.
Header files exported from a library go into the include off the top-level.
Header files for classes introduced in an application directory should never go
into include.

C++ features
------------
Earlier versions of BZFlag avoided certain features of C++ that have matured
enough to be widely and well supported.  These features are now permitted and
encouraged:
  bool			-- the boolean type and True and False are gone
  standard C++ library	-- use where appropriate (AList is gone)
  templates		-- use sparingly where appropriate

These should still be avoided:
  exceptions		-- still poorly supported on old lame compilers
  run-time typing	-- if you find yourself needing RTTI, its often a
			   sign of bad design. try to find another way to
			   implement your idea.

Multiple inheritance is strongly discouraged unless in the Java style of single
implementation inheritance and multiple interface inheritance.  Multiple
inheritance otherwise can quickly lead to very hard to understand code.

Formatting
----------
Everybody has their own style and has things they don't like about any other
style.  Well we can't have a zillion styles in the code. So follow the BZFlag
if you want your contribution included.  The source code serves for examples
but some rules:

  1)  indents are 2 characters. Tabs are 8 characters. There are vi and emacs
      settings in each file to adopt, enforce, remind, and encourage this
      convention. Suggestions welcome here for setting up other environments.
      Here are the lines that should be included at the end of source files:

	// Local Variables: ***
	// mode:C++ ***
	// tab-width: 8 ***
	// c-basic-offset: 2 ***
	// indent-tabs-mode: t ***
	// End: ***
	// ex: shiftwidth=2 tabstop=8

  2)  the opening brace of all blocks goes on the same line as the statement
      introducing it except for functions where it goes on the following line.
      the closing brace is indented like the statement except for functions
      where it aligns with the open brace.  for example:
        void foo()
        {
          for (;;) {
            if (expr) {
            }
          }
        }
  3)  an else clause goes on the same line (there are many bad examples):
        if (expr) {
        } else {
        }
	*note that old code had the else on the next line,
	 please clean this up wherever you may find it*
  4)  if *either* the if block or else block requires brackets then they both
      get brackets.  if neither require brackets then use brackets on both or
      neither at your discretion.
  5)  when using `delete' or `delete[]' don't bother to test the pointer if
      it's NULL first.  use new and delete rather than malloc and free.
  6)  data members should be usually private unless making plain old data.
      separate methods from data in class definitions with (possibly redundant)
      access specifiers.  public c'tors/d'tors should be first, followed by
      public member functions, protected member functions, private member
      functions, and data members.
  7)  macro names are all capitals, class names have the first letter of each
      word capitalized, all other names have the first letter of each word
      except the first capitalized.  only macros may use underscores except a
      leading underscore in a method parameter name is allowed to make it
      different from a member variable.
        #define FOO bar
        class MyClass {
        public:
            void        addStuff(int addMe, int _y) { y = addMe + y; }

        private:
            int         y;
        };
  8)  put spaces after statements followed by expressions and spaces around
      operators.  for example:
        if (a == b)
      not
        if(a==b)
  9)  Do not leave old commented code hanging around and do not submit patches
      with "// Added by foo on xx" comments. This is what version control
      software is for.

Violations of these rules in the existing code are not excuses to follow suit.
Non-conformant code may be fixed.  Patches to non-conformant code should follow
the non-conformant code's style if following the rules would cause an ugly
mess.


Testing
=======

If you have rendering problems, you might want to try setting:

LIBGL_ALWAYS_INDIRECT=1

On many systems this will force software rendering.  It will greatly
reduce the performance, but may assist in solving rendering issues.

A debug build may be specified via:

./configure --enable-debug

A profile build is specified via:

./configure --enable-profile


Sounds
======
Sounds are stored as 22.050KHz WAV files. We will likely move to OGG.


Images
======
Images are stored as .png (portable network graphics format). Compression,
but no additional options such as interlacing.


BZDB
====
BZDB is the generic name:value pair database within bzflag and bzfs. Its useful
for data that can be serialized to a string that needs to be accessible to
many areas of the code. It also provides facilities for saving persistant
pairs to the config file and downloading variables from the server.

BZDB is not an object broker, and isn't meant to be. If you have data within an
object that needs to be accessible from a number of places, but don't want to
pass the object around, you could store that data within BZDB (if accessed
often, such as game variables like gravity, you will need a cached version
anyway to avoid the overhead of lookup). Using BZDB adds unnecessary overhead
if objects generally keep their data hidden without needing persistant state.

Basically, if your data can be serialized to a string, and it makes sense to do
so (eg: config file option, game variable downloaded from server), use BZDB. If
you wanted an object broker, use a freakin' global.


Details -- common
=================
common.h	-- basic types and function declarations
ErrorHandler	-- a central place for printing errors (using printError())
TimeBomb	-- used for expiring development releases


Details -- geometry
===================
Intersect	-- 2D and 3D intersection functions
Ray		-- a 3D ray
ViewFrustum	-- a 3D viewing frustum


Details -- net
==============
Contains stuff for network handling.  This is in flux.  All platform specific
network code should end up in here.


Details -- obstacle
===================
Collision detection stuff.  Currently specific to BZFlag it handles boxes,
pyramids, teleporters, etc.  This should be rewritten to do generic 3D
collision detection on, at least, convex polyhedra.  It should also do
collision response.


Details -- ogl
==============
Wraps rendering state, texture maps, and texture fonts.  Currently it is OpenGL
specific but it could be made rendering library agnostic. There are a number of
places outside this library where OpenGL is used, though.  Ideally all
rendering library calls would go through a library agnostic interface.
However, OpenGL has the widest support of any rendering library so this is
pretty much a non-issue (unless someone wants to port to a game console).


Details -- platform
===================
Contains files to implement platform dependent versions of:
  BzfDisplay	-- event handling, screen size, and video format changing
  BzfVisual	-- wraps pixel format selection
  BzfWindow	-- window management, gamma control, mouse & joystick stuff
which handles:
  configuration file I/O
  text console output
  time and timers
  querying the user name
  environment variables
  signal handling
Configuration files are platform independent but their location is not.


Details -- scene
================
FIXME


Details -- mediafile
====================
Contains loaders for image and sound files


Details -- zlib
===============
zlib compression library for PNG decompression


Details -- Version numbers
==========================
The BZFlag versioning info is defined in:   include/version.h

There are a number of #defines in that file that define the Major, Minor,
and Revision versions, as well as the build type, build date, build user, and
network protocol version.

The BZFlag version number is in the format of:

  MajorVersion.MinorVersion.Revision

All "development" versions use an odd number for the minor version number.  All
"release" versions use an even number for the minor version, e.g.. 1.9.x is a
development version for a future 1.10 release.  Release versions also use even
revision version numbers. Maintainance work is done on odd numbered revisions
and releases are even numbered. For development versions the revision version
represents signifigant feature changes or stages in a development version, such
as a change to the network protocol or a move to a definate testing stage.  For
"release" versions, the revision represents patches or bug fixes to the base
release, but not new feature development.  This allows the developers to "leave
some room" for patches and emergency fixes to a release line.

The network protocol version is a string. When changes to the protocol are made
that render it incompatable with prior releases, the version number of the
protocol must be changed.  This is necessary to prevent incompatable clients
from atempting connections.  When a change is made, the network protocol version
needs to be set to the application version that is current at that time.  This
is also when the revistion of the application should be incremented.

The displayed application version includes additional information in the format:

  Major.Minor.Revision-BuildOS-BuildType-BuildDate

BuildOS is the operating system that the building system is running; on systems
that use the automake build system this is automatically generated.

BuildType is a string that represents the intended use of the build.  For
development releases, the build type is normally "CVS".  For testing releases,
the build type can be "testing", "beta", "releasecandidate", etc.  For final
release versions the build type should be "release".  The build type provides a
human readable keyword for the version and intended or expected stability of the
build.

BuildDate represents the date of the build in the format YYYYMMDD.  This string
is generated during runtime by the compiler's preprocessor __DATE__ macro.


Making a Release
================

In order to make a release, there are handful of steps that need to be
taken.  If any step fails to be completed successfully, the release
cannot proceed.  A checklist of items to be completed follows:

- All code is commited to CVS

- ChangeLog includes a comment for all items added since the previous
  release, preferably denoting who made what changes.

- Man files, bzfs_conf.html, and bzfs.conf files are updated with
  latest changes.

- Version numbers are updated to the next expected release number.
  This minimally includes updating README, configure.in, and
  ChangeLog.  Other platform-specific README.* files should also be
  verfied that all version numbers are up-to-date.

- Update package/win32/nsis/*.nsi (for windows) with appropriate
  version numbers 

- Perform a "make distcheck".  This will verify that a proper source
  distribution can be made.

- Tag the release.  Tag format should be consistent with the other
  tags using the format of vMAJOR_MINOR_RELEASE 
	e.g.cvs tag v1_10_2

- Perform a "cvs export -r vMAJOR_MINOR_RELEASE" from somehwere else
  in the filesystem to obtain a tagged version of the sources.

- Perform a "make dist" of that export to generate a source release
  tarball.

- Verify that the source tarball can be expanded, builds, and runs.

- Build platform-specific binaries from the source tarball.

- Post the source tarball and platform-specific binaries to
  SourceForge.

- Increment and commit the version number in the CVS source
  configure.in file so that later builds are immediately
  distiguished..

- Notify the following (at least):

bzflag-users@lists.SourceForge.net
news on http://BZFlag.org/
news on http://sourceforge.net/projects/bzflag/
http://slashdot.org/
http://freshmeat.net/projects/bzflag/ (rank BZFlag here!)
http://happypenguin.org/ (BZFlag is now #2 in the ratings!)
news@linuxgames.com - http://LinuxGames.com/
John Gowin <jgowin@linuxorbit.com> (thanx for the review!)
jd@linuxgaming.co.uk
updates@superdownloads.com.br
mail -s add LSM@execpc.com < bzflag.lsm

